package com.alushpigan;

public abstract class Report {
    public abstract void makeReport();
    public void userReport (User user){
        makeReport();
        System.out.println("for user object "+user.getfName()+" "+user.getlName());

    }
    public void userReport (String fname, String lname){
        makeReport();
        System.out.println("for user "+fname+" "+lname);

    }
}
