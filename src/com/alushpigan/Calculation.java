package com.alushpigan;

public class Calculation {

    public static void main(String[] args) {
        String fName="Sveta";
        String lName="Petrova";
        User user=new User("Ivan", "Sidorov");
        Report cmr = new CalculateMonthReport();
        cmr.userReport(user);
        cmr = new CalculateYearReport();
        cmr.userReport(fName,lName);
    }
}
