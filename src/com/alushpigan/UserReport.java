package com.alushpigan;

public class UserReport {
    public UserReport (User user){
        System.out.println("Report for user object "+user.getfName()+" "+user.getlName());
    }
    public UserReport (String fname, String lname){
        System.out.println("Report for user "+fname+" "+lname);
    }
}
