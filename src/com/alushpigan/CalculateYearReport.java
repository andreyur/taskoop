package com.alushpigan;

public class CalculateYearReport extends YearReport implements CheckSomeReport {
    @Override
    public void userReport(User user) {
        super.userReport(user);
        checkReport();
    }
    @Override
    public void userReport(String fName, String lName) {
        super.userReport(fName, lName);
        checkReport();
    }
    @Override
    public void checkReport() {
        System.out.println("The year report is checked");
    }
}
